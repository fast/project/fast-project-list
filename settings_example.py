from projector.settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'FAKE_FAKEjunkvvvassvdv'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': os.environ['DJANGO_DB_NAME'],
#         'USER': os.environ['DJANGO_DB_USER'],
#         'PASSWORD': os.environ['DJANGO_DB_PASSWORD'],
#         'HOST': 'db',
#     }
# }

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

EMAIL_USE_TLS = True
EMAIL_HOST_USER = '-'
EMAIL_HOST = '-'
EMAIL_PORT = 25
EMAIL_HOST_PASSWORD = ''

# #INSTALLED_APPS.append('oidc_auth')
# AUTHENTICATION_BACKENDS = [
#     'oidc_auth.backends.OIDCBackend',
# ]
# LOGIN_URL = 'oidc_auth:oidc_login-duo'
# LOGOUT_URL = 'oidc_auth:oidc_logout-duo'
# OIDC_CLIENTS = {
#     'duo': {
#         "auth_server": os.environ['OIDC_AUTH_SERVER'],
#         "client_id": os.environ['OIDC_CLIENT_ID'],
#         "client_secret": os.environ['OIDC_CLIENT_SECRET'],
#         "callback_url": os.environ.get('OIDC_CALLBACK', '/oidc/duo/callback/'),
#         "claims": "openid email profile group given_name family_name",
#         "username_claim": "winaccountname",
#         "claim_mapping": {
#             "first_name": "given_name",
#             "last_name": "family_name",
#             "email": "email",
#         },
#         "group_claim": "group",
#         "group_to_flag_mapping": {
#             "is_staff": ["fast"],
#             "is_superuser": ["fast-server-admin"],
#         },
#         "auto_create_user": True,
#         "use_django_session_expiry": True,
#     },
# }
