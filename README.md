Production
----------

```
ssh root@fast-01-prod.fast.uwaterloo.ca
salt-call state.apply
```

Debug
-----

To view log files use `journacld`

```
journacld -f
```

Application code is found in: `/srv/<appname>/src`


DEV
---

vscode open devcontainer

```
pip install -r ./requirements.txt
.devcontainer/command.sh
```