import {computed} from 'vue';
import QuickInputs from "../components/quick-inputs.js";

const template = /*html*/`
    <h3>Project Impact</h3>
    <p>To help weigh the potential impact of downtime, breaches, or critical issues, please fill out the
    following information about your project:</p>

    <hr class="my-4"/>
    
    <p>Give a <b>brief</b> summary of:
        <ul class="mb-1">
            <li>what the project does</li>
            <li>when the heaviest usage occurs</li>
            <li>who would be most impacted if the software went down</li>
        </ul>
    <QuickTextArea v-model="model.fields.summary" :err="model.errors?.summary" />
    </p>

    <hr class="my-4"/>

    <p>Who makes up the userbase for this project? Please check all that apply:</p>
    <QuickCheckbox v-model="model.fields.public_users" group_class="mb-1" label="Public" />
    <QuickCheckbox v-model="model.fields.student_users" group_class="mb-1" label="Students" />
    <QuickCheckbox v-model="model.fields.staff_users" group_class="mb-1" label="Staff Members" />
    <QuickCheckbox v-model="model.fields.faculty_users" group_class="mb-1" label="Faculty Members" />
    <QuickCheckbox v-model="model.fields.external_users" group_class="mb-1" label="External Contacts (e.g. external researchers)" />
    <QuickCheckbox v-model="model.fields.api_users" group_class="" label="API Processes" />

    <hr class="my-4"/>

    <p >In the span of a typical "busy" term, roughly how many <b>unique</b> users
    will access or use the project?
    <br/>
    <em>(Pick the option that you're confident you don't exceed)</em>
    </p>
    <QuickSelect v-model="model.fields.num_users" :err="model.errors?.num_users">
        <option value="10">~10</option>
        <option value="100">~100</option>
        <option value="1000">~1000</option>
        <option value="10000">Up to 10k</option>
        <option value="10001">More than 10k</option>
        <option value="-1">Honestly no idea</option>
    </QuickSelect>

    <hr class="my-4"/>

    <p>In your own estimation, how critical is this project? Consider answering this question in the context of <em>the primary maintainers not being available to fix issues</em>.</p>
    <QuickRadioOptions v-model="model.fields.critical_level" :err="model.errors?.critical_level"
        :options="[
            [5, 'CRITICAL: Failure would cause catastrophic issues (eg. QUEST, ADFS)'],
            [4, 'HIGH: Failure can be worked around with signifigant coordinated effort'],
            [3, 'MEDIUM: System can be replaced by alternate process / team within 2 weeks'],
            [2, 'LOW: The software is useful, but is not necessary'],
            [0, 'NOT USEFUL: This software actively hinders the business of the University'],
        ]"
    />

    <hr class="my-4"/>

    <p>Does you project store or generate any of the following types of records or files:
        <ul class="mb-2" >
            <li>private personal information (grades, health data, addresses, etc)</li>
            <li>private business data (exam questions, research, etc)</li>
            <li>any form of financial data</li>
        </ul>
        If so, provide a <b>brief</b> overview here:
    </p>
        <QuickTextArea v-model="model.fields.private_information" :err="model.errors?.private_information" />
    <div class="alert alert-warning">
    If you are storing private information, it is  recommended <em>(technically required)</em> that you complete an 
    <em><a href="https://uwaterloo.ca/privacy/information-risk-assessment" target="_blank">
    Official Information Risk Assessment.
    </a></em>
    <QuickCheckbox 
        v-model="model.fields.risk_assessment"
        label="This project has undergone an official risk assessment"
        group_class="mb-1 mt-2"/>

    </div>
    
    <hr class="my-4"/>

`

export default {
    components: {...QuickInputs},
    props: ['modelValue'],
    emits: ['update:modelValue'],
    template,
    setup(props, {emit}){
        return {
            model: computed({
                get: ()=>props.modelValue,
                set: (val)=>emit('update:modelValue', val)
            })
        }
    }
}
