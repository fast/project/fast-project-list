import {createApp, ref} from 'vue';
import ChangeManagement from './change-management.js';
import Impact from './impact.js';
import Toasts, {makeToast} from '../components/toasts.js';

const template = /*html*/`
<Toasts />
<div class="mx-auto" style="max-width: 800px;">
    <ul class="nav nav-tabs">
    <li class="nav-item" v-for="p in pages">
        <a class="nav-link" :class="page==p ? 'active' : ''" @click="page=p" href="#">
        {{p}}
        <span v-if="specs[p]?.pk" class="text-success">🗸</span>
        </a>
    </li>
    </ul>

    <div class="border border-top-0 p-4">
        <div v-if="page&&specs[page]?.pk" class="mb-4">
            <em class="text-muted">Last updated {{specs[page].fields.last_update}} by {{specs[page].fields.last_update_author}}</em>
        </div>
        <div v-if="!page">
            <h3 class="mb-1">Welcome to the FAST Specification System</h3>
            <div class="small text-muted mb-4"><em>(Working title)</div>
            <p>Our goal is to help establish helpful non-restrictive project <b>"good practices"
            to assist developers in supporting one anothers' projects.</p>
            <p>Filling these forms out has lots of benefits:
                <ul>
                <li>You can export all the info as a report for your manager!</li>
                <li>Makes internal audits easier!</li>
                <li>Helps you ensure all your projects are practicing good standards!</li>
                <li>Helps us all support one another's stuff in emergency situations</li>\
                <li>Makes you cool</li> 
                </ul>
            </p>

            <p>Filling out these forms <em>won't be used against you.</em> Indicating you have secrets in your repos
            or aren't yet AODA compliant simply helps us get <em>statistics</em>.</p>
            <p>Keep in mind that most projects, especially small ones, likely won't check <em>all</em> the boxes. <b>That's OK!</b>

            <p>To get started, click any of the tabs above! In the future we hope to make this entire process super automatable to reduce how much work this takes.</p>
        </div>
        <Impact v-else-if="page=='Impact'" v-model="specs['Impact']" />
        <ChangeManagement v-else-if="page=='Change Management'" v-model="specs['Change Management']"/>
        <div v-else>
            <div class="alert alert-danger">
                NO WIDGET BUILT FOR THIS SPEC
            </div>
        </div>

        <div v-if="page">
            <button class="btn btn-primary" @click="saveSpec" >Save</button>
        </div>
    </div>
</div>

`
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const app = createApp({
    components: {
        Toasts,
        ChangeManagement,
        Impact
    },
    template,
    setup(){
        const page = ref(null)
        const specs = ref(window.CONTEXT.specs)
        const saveSpec = async() => {
            let data = new FormData()
            Object.entries(specs.value[page.value].fields).forEach(x=>{
                data.append(x[0], x[1])
            })
            let resp = await fetch(
                `/${window.CONTEXT.project_id}/save_specification/${page.value}/`,
                {
                    method: 'POST',
                    mode: "same-origin",
                    headers: {
                        // "Content-Type": "multipart/form-data",
                        "X-CSRFToken": getCookie("csrftoken")
                    },
                    body: data
                },
            )
            if (!resp.ok){
                let errors = {}
                try {
                    errors = await resp.json()
                    specs.value[page.value].errors = errors
                    makeToast("One or more invalid inputs. Please correct errors", 'danger')

                } catch {
                    makeToast("Unknown error occured (500)", 'danger')
                }
            }
            else {
                let success = await resp.json()
                specs.value[page.value] = success
                makeToast(`Successfully updated "${page.value}"`, 'success')
            }
            window.scrollTo(0,0);

        }
        return {
            pages: [
                'Impact',
                'Change Management',
            ],
            page,
            specs,
            saveSpec
        }
    }
})
app.mount("#app")
