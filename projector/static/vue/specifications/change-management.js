import {ref, computed} from 'vue';
import QuickInputs from '../components/quick-inputs.js';

const template = /*html*/`
<div>
    <h3>Change Management</h3>
    <p>For basic supportability all campus projects should be using some form of <b>version control</b>.
    If you're working on projects that haven't started using this technology yet, <a target="blank" href="https://about.gitlab.com/topics/version-control/">here's a link to GitLab's writeup about what version control is</a>.
    </p>
    <p>
        The University of Waterloo provides <a href="https://git.uwaterloo.ca" target="_blank">a local GitLab Instance</a> 
        used for many internal software projects. For documentation projects consider a wiki or request a Confluence site from IST.
    </p>

    <p>Filling out the following form will help other developers find and support your project if needed.</p>


    <hr class="my-4"/>

    <QuickInput 
        v-model="model.fields.repo_url"
        :err="model.errors?.repo_url"
        label="Primary Repository URL" 
        placeholder="https://git.uwaterloo.ca/...."
        help="must be a URL. For non-code projects consider linking to documentation or configuration repository."
    />
    <QuickTextArea
        v-model="model.fields.issue_locations"
        :err="model.errors?.issue_locations"
        label="Issue / Change Tracking locations"
        placeholder="URL for repository issues tracker / 'steves notebook' / JIRA queues / etc. (comma separated)"
        help="comma-separate for multiple locations"
    />


    <hr class="my-4"/>


    <h5>Project Access</h5>
    <p>To ensure a project never reaches a state where assets are inaccessible, every project should have at least <b>two individuals with maintainer/owner status</b>.</p>
    <p>"Maintainer" status usually implies the maximum level of permissions on repositories.
    This isn't necessarily someone who is capable of working on the project, but rather someone who can
    grant access to others and has access to all project settings.
    </p>
    <p><b>Provide the email addresses of 2 contacts with "maintainer" access to the primary repository:</b></p>
    <QuickInput  v-model="model.fields.maintainer_1" :err="model.errors?.maintainer_1" placeholder="maintainer1@uwaterloo.ca" />
    <QuickInput  v-model="model.fields.maintainer_2" :err="model.errors?.maintainer_2" placeholder="maintainer2@uwaterloo.ca" />
    <p><em>If your project currently only has yourself as a maintainer, consider granting access to your manager or the FAST admins service account.</em></p>


    <hr class="my-4"/>


    <h5>Good Practices</h5>
    <p>The following questions outline some much-appreciated good practices to include in your repository. While none of these are a requirement
    consider revising repositories that don't satisfy these questions and adding the outlined information.</p>
    <p><em>Most of these items pertain specifically to software projects.</em></p>

    <div class="card">
    <div class="card-body">
        <p>Does your repo contain sufficient instructions (or links to instructions) to set up,
        from scratch, a <b>local development instance</b>?</p>
        <p class="mb-0">If possible, try having someone from
        a differet team set up a development instance of your tool using only
        these instructions.</p>
    </div>
    <div class="card-footer">
        <QuickCheckbox
            v-model="model.fields.has_dev_text"
            label="Repo contains development setup instructions"
            group_class="mb-0"
        />
    </div></div>

    <div class="card mt-4">
    <div class="card-body">
        <p>Does your primary repository have instructions (or links to instructions) to deploy a <b>production instance</b>
        of the project?</p>
        <p>If possible, try re-deploying your project on a fresh VM and make sure the instructions
        still work!</p>
        <p class="mb-0">Note: An increasingly common way to provide simply deploy instructions is to include a <code>docker-compose.yml</code> and <code>Dockerfile</code>
        in the repository. Even if other deveopers don't actively use Docker, these two files provide easy-to-read
        instructions on going from zero to production with all the dependencies!</p>
    </div>
    <div class="card-footer">
        <QuickCheckbox
            v-model="model.fields.has_deploy_text"
            label="Primary repo contains production deployment instructions"
            group_class="mb-0" 
        />
    </div>
    </div>

    <div class="card mt-4">
    <div class="card-body">
    <p>Does your primary repository have a <code>CONTRIBUTING.md</code> (or equivalent documentation)? It should include
    information for other developers on the contribution process for your project, how to run tests (if any),
    and how you expect issues to be dealt with.</p>
    <p class="mb-0"> It can also be useful to highlight any unique coding practices explored in the project, or outline
    where key files are.</p>
    </div>
    <div class="card-footer">
    <QuickCheckbox 
        v-model="model.fields.has_contributor_text"
        label="Primary repo documents contributing process" 
        group_class="mb-0" 
    />
    </div>
    </div>

    <div class="card mt-4">
    <div class="card-body">
        <p>It is generally considered best practice to not keep any secrets (passwords, api keys, etc) in a repository, even if
        that repository is private.</p>
        <p>If secrets <em>are</em> stored within a repository, they should be encrypted in such a way that they cannot be extracted by
        malicious parties.</p>
        <p class="mb-0">If you currently have secrets in your repository, consider <a target="_blank" href="https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository">this guide</a>
        to help remove them from the commit history</p>
    </div>
    <div class="card-footer">
        <QuickCheckbox
            v-model="model.fields.has_no_secrets"
            label="There are NO unencrypted secrets in the primary repo"
            group_class="mb-0"
        />
    </div>
    </div>

    <div class="card mt-4">
    <div class="card-body">
        <p>Do you have a LICENSE attached to the project?</p>
        <p class="mb-0"><em>Note @ 2023-02-13: we hope to have some clear recommendations on this very soon</em></p>
    </div>
    <div class="card-footer">
        <QuickCheckbox
            v-model="model.fields.has_license"
            label="Primary repo contains a LICENCE"
            group_class="mb-0" 
        />
    </div>
    </div>


    <hr class="my-4"/>

</div>
`

export default {
    components: {...QuickInputs},
    props: ['modelValue'],
    emits: ['update:modelValue'],
    template,
    setup(props, {emit}){
        return {
            model: computed({
                get: ()=>props.modelValue,
                set: (val)=>emit('update:modelValue', val)
            })
        }
    }
}
