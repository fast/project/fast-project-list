const InputWrap = {
    props:['label', 'help', 'err', 'group_class'],
    template: /*html*/`
    <div class="form-group" :class="group_class||''">
        <label class="" v-if="label!==undefined">
            <span><b>{{label}}</b></span>
        </label>
        <slot/>
        <div v-if="err" class="invalid-feedback">
            <ul class="" v-if="err.constructor === Array">
                <li v-for="e in err">{{e.message}}</li>
            </ul>
            <span v-else>{{err}}</span>
        </div>
        <small v-else-if="help" class="form-text text-muted">
            {{help}}
        </small>
    </div>`
}

const QuickInput = {
    components: {InputWrap},
    props: ['label', 'modelValue', 'err', 'help'],
    emits: ['update:modelValue'],
    inheritAttrs: false,
    template: /*html*/`
        <input-wrap :label="label" :err="err" :help="help">
            <input 
                v-bind="$attrs" 
                class="form-control"
                :class="err&&'is-invalid'" 
                :value="modelValue" 
                @input="e=>$emit('update:modelValue', e.target.value)" 
            />
        </input-wrap>`
}

const QuickTextArea = {
    components: {InputWrap},
    props: ['label', 'modelValue', 'err', 'help'],
    emits: ['update:modelValue'],
    inheritAttrs: false,
    template: /*html*/`
        <input-wrap :label="label" :err="err" :help="help">
            <textarea 
                v-bind="$attrs"
                class="form-control" 
                :class="err&&'is-invalid'" 
                :value="modelValue"
                @input="e=>$emit('update:modelValue', e.target.value)"
            />
        </input-wrap>`
}

const QuickSelect = {
    components: {InputWrap},
    props: ['label', 'modelValue', 'err', 'help'],
    emits: ['update:modelValue'],
    inheritAttrs: false,
    template: /*html*/`
        <input-wrap :label="label" :err="err" :help="help">
            <select
                v-bind="$attrs"
                :value="modelValue"
                class="form-control"
                :class="err&&'is-invalid'" 
                @change="e=>$emit('update:modelValue', e.target.value)"
            >
                <slot />
            </select>
        </input-wrap>`
}

const QuickRadioOptions = {
    components: {InputWrap},
    props: ['label', 'modelValue', 'err', 'help', 'options'],
    emits: ['update:modelValue'],
    inheritAttrs: false,
    template: /*html*/`
        <input-wrap :label="label" :err="err" :help="help">
            <div class="custom-control custom-radio" v-for="op in options" :class="err ? 'is-invalid' : ''" 
                    @click="$emit('update:modelValue', op[0])"
                >
                <input class="custom-control-input" type="radio" :value="op[0]"
                    :class="err ? 'is-invalid': ''"
                    :checked="modelValue==op[0]"
                />
                <label class="custom-control-label">{{op[1]}}</label>
            </div>
        </input-wrap>`
}

const QuickCheckbox = {
    props: ['label', 'modelValue', 'err', 'help', 'group_class'],
    emits: ['update:modelValue'],
    inheritAttrs: false,
    template: /*html*/`
        <div class="form-group" :class="group_class||''">
            <div class="custom-control custom-checkbox" @click="e=>{$emit('update:modelValue', !!!modelValue)}">
                <input type="checkbox" class="custom-control-input"
                    v-bind="$attrs"
                    v-model="modelValue"
                    :class="err&&'is-invalid'" 
                />
                <label class="custom-control-label">{{label}}</label>
            </div>
            <div v-if="err" class="invalid-feedback">
                <ul class="" v-if="err.constructor === Array">
                    <li v-for="e in err">{{e.message}}</li>
                </ul>
                <span v-else>{{err}}</span>
            </div>
            <small v-else-if="help" class="form-text text-muted">
                {{help}}
            </small>
        </div>`
}

export default {
    QuickInput,
    QuickSelect,
    QuickTextArea,
    QuickCheckbox,
    QuickRadioOptions,
}