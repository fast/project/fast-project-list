import {reactive, onMounted, onBeforeUnmount, createApp} from 'vue';

const toast_list = reactive({toasts:[]})

export const makeToast = (message, color='success') => {
    toast_list.toasts.push({
        id: new Date().getTime(),
        message,
        color
    })
}

const Toast = {
    props: ['message', 'color'],
    emits: ['delete'],
    setup(_props, {emit}){
        let timer = null
        onMounted(()=>{
            timer = setTimeout(()=>emit('delete'), 6000)
        })
        onBeforeUnmount(()=>{
            clearTimeout(timer)
        })
        return {}
    },
    template: `
        <div class="alert" @click="$emit('delete')" :class="'alert-'+color"><div><span>{{message}}</span></div></div>`,

}

export default {
    components: {Toast},
    setup(){
        return {
            toast_list,
            rem: (id)=>{
                toast_list.toasts = toast_list.toasts.filter(x=>x.id!=id)
            }
        }
    },
    template: `<div style="position: fixed; top: 68px; right: 8px; z-index: 1040;">
        <toast v-for="t in toast_list.toasts" :message="t.message" :color="t.color" :key="t.id"  @delete="()=>rem(t.id)"/>
    </div>`
}
