(function($) {
    $(document).on('formset:added', function(event, $row, formsetName) {
    if (formsetName == 'trackeddocument_set'){
       let title_field = $row.find('input[id$="title"]') 
       let slug_field = $row.find('input[id$="slug"]') .data('dependency_list', ['title'])
       slug_field.prepopulate(
            ['#' + title_field[0].id], 256, false
        );
       }
    });

    $(document).on('formset:removed', function(event, $row, formsetName) {
        // Row removed
    });
})(django.jQuery);