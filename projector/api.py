# Being lazy and putting everything in one file for simlicity for now
from rest_framework import serializers, viewsets, mixins, pagination
from projector.models import Project, Contact


class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 1000
    page_size=200

class ContactSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.CharField()
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'email', 'contact_for']

class PublicProjectSerializer(serializers.ModelSerializer):
    groups = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name'
    )
    contacts = ContactSerializer(many=True, read_only=True, source="contact_set")
    class Meta:
        model = Project
        fields = [
            'id',
            'title',
            'tagline',
            'slug',
            'url',
            'groups',
            'contacts',
        ]

class PublicProjectViewSet(
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = PublicProjectSerializer
    pagination_class = LargeResultsSetPagination
    queryset = Project.objects.filter(
        visible=True
    ).order_by('title')

