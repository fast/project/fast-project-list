from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, Http404, HttpResponse
from django.views import generic
from .models import Project, Contributor, Contact, Group
# TrackedDocument
from .models import (
    ImpactSpec,
    ChangeManagementSpec
)
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Q
from .permissions import user_projects_queryset, can_edit_project
import json
from markdown import markdown
from django.db.models import Q, BooleanField, Case, When



# Some tools we use later
specList = {
    'Impact': ImpactSpec,
    'Change Management': ChangeManagementSpec,
}

def get_related(model, related_model):
    field = model._meta.get_field(related_model._meta.model_name)
    return field.name

specQuery = Q()
for x in specList.values():
    specQuery |= Q(**{get_related(Project, x)+'__isnull': False})

def annotateHasSpec(queryset):
    return queryset.annotate(has_spec=Case(
        When(specQuery, then=True),
        default=False,
        output_field=BooleanField()
    ))

def compileSpecsText(project):
    out = f""
    for k, v in specList.items():
        i = v.objects.filter(project=project).first()
        if i:
            out += f"### {k} [{i.last_update.strftime('%Y-%m-%d')}]\n"
            out += i.as_text()
    return out


def getFilteredProjects(for_user=None, group=None, tech=None, search=None):
    if for_user:
        queryset = user_projects_queryset(for_user)
    else:
        queryset = Project.objects.all()
    if search:
        queryset = queryset.filter(
            Q(title__icontains=search) |
            Q(groups__name__icontains=search)
        )
    if tech:
        queryset = queryset.filter(
            technologies__slug__iexact=tech
        )
    if group:
        queryset = queryset.filter(
            groups__slug__iexact=group
        )
    return queryset

def getProjectFilters(request):
        return dict(
            for_user = request.user if (request.user.is_authenticated and request.GET.get('user', None)) else None,
            search = request.GET.get('q', None),
            tech = request.GET.get('tech', None),
            group = request.GET.get('group', None),
        )


class IndexView(generic.ListView):
    model = Project
    template_name = 'index.html'
    context_object_name = 'projects'
    paginate_by = 20

    def get_queryset(self):
        filters = getProjectFilters(self.request)
        filtered = len([1 for x in filters.values() if x]) > 0

        queryset = getFilteredProjects(**filters)

        # Ordering
        ordering = self.request.GET.get('order_by', 'title')
        allowed_ordering = [
            'title',
            'scope'
        ]
        if ordering not in allowed_ordering and\
            ordering[1:] not in allowed_ordering:
                ordering = 'title'
        
        queryset = queryset.order_by(ordering)
        self._filtered = filtered
        
        if self.request.user.is_authenticated:
            queryset = annotateHasSpec(queryset)
        else:
            queryset = queryset.filter(visible=True)
        return queryset.distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filtered'] = self._filtered
        return context


class DetailView(generic.DetailView):
    model = Project
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project = context['project']
        roles = Contributor.ROLES

        contributors = []
        for role in roles:
            new_item = {
                "label": role[0],
                "members": project.contributor_set.filter(role=role[0])
            }
            contributors.append(new_item)


        context['contributors'] = contributors
        if self.request.user.is_authenticated:
            context['specs'] = markdown(compileSpecsText(project))
            context['can_edit'] = can_edit_project(self.request.user, project)
        return context

# class DocumentView(generic.DetailView):
#     template_name = 'view-document.html'
#
#     def get_object(self):
#         queryset = TrackedDocument.objects.all()
#         try:
#             obj = queryset.get(
#                 project__slug=self.kwargs['slug'],
#                 slug=self.kwargs['doc_slug']
#             )
#         except queryset.model.DoesNotExist:
#             raise Http404
#         return obj

def about(request):
    return render(request, 'about.html')


@login_required
def spec_dump(request):
    filters = getProjectFilters(request)
    queryset = getFilteredProjects(**filters).order_by('title')

    mode = request.GET.get('mode', 'html')
    if mode not in ['html', 'json', 'md']:
        mode = 'html'

    if mode == 'json':
        out_obj = {}
        for project in queryset:
            ob = {
                'title': project.title,
                # Add contributor stuff here
            }
            specs = {}
            for k, v in specList.items():
                item = v.objects.filter(project=project).first()
                if item:
                    specs[k] = item.as_json()['fields']
            ob['specs'] = specs
            out_obj[project.slug] = ob
        
        return JsonResponse(out_obj)

    # Construct the report...
    output_str = ''
    for project in queryset:
        output_str += f'# {project.title} \n\n'
        txt = compileSpecsText(project)
        if not txt:
            txt = '*no information for this project*'
        output_str += txt
        output_str += '\n\n --- \n\n'
    
    if mode == "html":
        return render(request, 'spec_dump.html', {'content': markdown(output_str)})
    
    if mode == 'md':
        return HttpResponse(output_str, content_type="text/plain")



@login_required
def specifications(request, pk=None, slug=None):
    q = {"pk": pk} if pk else {"slug": slug}
    project = get_object_or_404(user_projects_queryset(request.user), **q)


    specs = {}
    for k, v in specList.items():
        item = v.objects.filter(project=project).first() or v()
        specs[k] = item.as_json()


    return render(request, 'specifications.html', {
        "project": project, 
        "js_context": {
            "specs": specs,
            "project_id": project.id,
        }
    })

@login_required
def save_specification(request, spec_key, pk=None, slug=None):
    q = {"pk": pk} if pk else {"slug": slug}
    project = get_object_or_404(user_projects_queryset(request.user), **q)

    spec_model = specList[spec_key]
    item = spec_model.objects.filter(project=project).first() or spec_model(project=project)

    form = item.as_form(request.POST)
    if form.is_valid():
        i = form.save(commit=False)
        i.last_update_author = request.user
        i.save()
        return JsonResponse(i.as_json())
    else:
        return JsonResponse(json.loads(form.errors.as_json()), status=400)
