from django import template
import urllib, hashlib
import markdown
from markdown.extensions.toc import TocExtension
from django.utils.safestring import mark_safe

register = template.Library()

@register.inclusion_tag('components/tech_badge.html')
def tech_badge(technology):
    class_color = {
        'LANGUAGE': 'badge-info',
        'BACKEND': 'badge-primary',
        'FRONTEND': 'badge-warning',
        'PLUGIN': 'badge-success'
    }.get(technology.category, 'badge-secondary')

    return {
        'class_color': class_color,
        'tech': technology,
    }

@register.inclusion_tag('components/group_search.html')
def group_search(group):
    return {
        'group': group,
    }

@register.inclusion_tag('components/user_avatar.html')
def user_avatar(user, size='125px'):
    avatar = user.avatar
    if not avatar:
        avatar_url = "https://www.gravatar.com/avatar/"\
            + hashlib.md5(user.email.lower().encode('utf-8')).hexdigest()\
            + "?d=retro&r=PG&size=200"
    else:
        avatar_url = avatar.url

    return {
        'fullname': '{} {}'.format(user.first_name, user.last_name),
        'avatar_url': avatar_url,
        'size': size
    }


@register.filter(name='markdown', is_safe=True)
def to_markdown(value):
    return mark_safe(markdown.markdown(value))

@register.filter(name='markdown_with_toc', is_safe=True)
def to_markdown_with_toc(value):
    if not '[TOC]\n\n' in value:
        value = '[TOC]\n\n' + value
    return mark_safe(markdown.markdown(value, extensions=[TocExtension()]))