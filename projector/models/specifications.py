from django.db import models
from django.contrib.auth import get_user_model
from django import forms
from django.core import serializers
import json


class Specification(models.Model):
    max_score = 5
    last_update = models.DateTimeField(auto_now=True)
    last_update_author = models.ForeignKey(
        get_user_model(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    score = models.IntegerField(default=0)
    comments = models.TextField(blank=True)
    project = models.OneToOneField('projector.Project', on_delete=models.CASCADE)

    def _compute_score(self):
        # Write a function to compute score property
        return 0
    
    def save(self, *args, **kwargs):
        self.score = self._compute_score()
        return super().save(*args, **kwargs)
    
    def as_text(self):
        return "as_text function not provided"
    
    def as_json(self):
        val =  json.loads(serializers.serialize('json', [self]))[0]
        if self.last_update_author:
            val['fields']['last_update_author'] = self.last_update_author.username
        return val

    def as_form(self, *args, **kwargs):
        class MForm(forms.ModelForm):
            class Meta:
                model = self.__class__
                exclude = [
                    'id', 'last_update', 'last_update_author',
                    'score', 'comments', 'project',
                ]
        return MForm(*args, instance=self, **kwargs)

    class Meta:
        abstract=True


class ImpactSpec(Specification):

    CRITICAL_LEVEL_OPTIONS = (
        (5, 'CRITICAL'),
        (4, 'HIGH'),
        (3, 'MEDIUM'),
        (2, 'LOW'),
        (0, 'NOT USEFUL'),
    )
    NUM_USERS_OPTIONS = (
        (10, '~10'),
        (100, '~100'),
        (1000, '~1000'),
        (10000, 'less than 10k'),
        (10001, '10k+'),
        (-1, 'unknown'),
    )

    summary = models.TextField()

    public_users = models.BooleanField(default=False)
    student_users = models.BooleanField(default=False)
    staff_users = models.BooleanField(default=False)
    faculty_users = models.BooleanField(default=False)
    external_users = models.BooleanField(default=False)
    api_users = models.BooleanField(default=False)

    num_users = models.IntegerField(choices=NUM_USERS_OPTIONS)
    critical_level = models.IntegerField(choices=CRITICAL_LEVEL_OPTIONS)
    private_information = models.TextField(blank=True)
    risk_assessment = models.BooleanField(default=False)

    def as_text(self):
        users = {
            "public": 'public_users',
            "students": 'student_users',
            "staff": 'staff_users',
            "faculty": 'faculty_users',
            "externals": 'external_users',
            "apis": 'api_users',
        }
        users = ", ".join([k for k, v in users.items() if getattr(self, v)])
        return f"""
**Summary:** {self.summary}

- **Used by:** {users}
- **Number of Users:** {self.get_num_users_display()}
- **Critical Level**: {self.get_critical_level_display()}

{'**Private Info:** '+self.private_information if self.private_information.strip() else ''}

{'**this project has completed a risk assessment**' if self.risk_assessment else ''}
"""


class ChangeManagementSpec(Specification):
    repo_url = models.URLField(
        max_length=1024,
    )
    issue_locations = models.TextField(
        blank=True
    )
    maintainer_1 = models.EmailField()
    maintainer_2 = models.EmailField(blank=True)

    has_dev_text = models.BooleanField(
        default=False
    )
    has_deploy_text = models.BooleanField(
        default=False
    )
    has_contributor_text = models.BooleanField(
        default=False
    )
    has_license = models.BooleanField(
        default=False
    )
    has_no_secrets = models.BooleanField(
        default=False
    )

    def as_text(self):
        issue_locations = '' if not self.issue_locations else f"**Issues:** {self.issue_locations}"
        bools = ['✓ ' + ' '.join(x.split('_')).title() for x in [
            'has_dev_text',
            'has_deploy_text',
            'has_contributor_text',
            'has_license',
            'has_no_secrets'
        ] if getattr(self, x)]
        bools = '' if not len(bools) else ('**Good Practice:** \n\n- ' + '\n- '.join(bools))
        return f"""
**Repo URL:** [{self.repo_url}]({self.repo_url})

{issue_locations}

**Maintainer access to repo:**

- [{self.maintainer_1}](mailto:{self.maintainer_1})
- [{self.maintainer_2}](mailto:{self.maintainer_2})

{bools}
"""