from django.db import models
from django.contrib.auth.models import AbstractUser
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
import hashlib


class User(AbstractUser):
    avatar = ProcessedImageField(
        upload_to='avatars',
        processors=[ResizeToFill(200, 200)],
        format='JPEG',
        options={'quality': 90},
        blank=True,
        null=True,
    )
    contributed_to = models.ManyToManyField(
        'projector.Project',
        through='projector.Contributor',
        related_name="contributor_users"
    )
    contact_for = models.ManyToManyField(
        'projector.Project',
        through='projector.Contact',
        related_name="contact_users"
    )



class UserLink(models.Model):
    user = models.ForeignKey(
        'projector.User',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    override_first_name = models.CharField(max_length=250, blank=True)
    override_last_name = models.CharField(max_length=250, blank=True)
    override_email = models.EmailField(blank=True)
    project = models.ForeignKey(
        'projector.Project',
        on_delete=models.CASCADE
    )
    order_with_respect_to = 'project'

    class Meta:
        abstract = True

    @property
    def username(self):
        return self.user.username

    @property
    def first_name(self):
        if self.user:
            return self.user.first_name
        else:
            return self.override_first_name

    @property
    def last_name(self):
        if self.user:
            return self.user.last_name
        else:
            return self.override_last_name

    @property
    def email(self):
        if self.user:
            return self.user.email
        else:
            return self.override_email

    @property
    def avatar(self):
        if self.user:
            return self.user.avatar
        else:
            return None
    @property
    def avatar_url(self):
        if not self.avatar:
            return "https://www.gravatar.com/avatar/"\
                + hashlib.md5(self.email.lower().encode('utf-8')).hexdigest()\
                + "?d=retro&r=PG&size=200"
        else:
            return self.avatar.url


class Contributor(UserLink):
    contributed_to = models.TextField(
        help_text="Quick summary of contributions, ex: co-op winter 2021, UX design, etc."
    )
    ROLES = (
        ('Manager', 'Manager'),
        ('Stakeholder', 'Stakeholder'),
        ('Developer', 'Developer'),
        ('Co-op', 'Co-op'),
    )
    role = models.CharField(
        max_length=100,
        choices=ROLES,
        blank=True,
    )
    show_email = models.BooleanField(default=False)

    def __str__(self):
        if self.override_first_name:
            n = 'Manual'
        else:
            n = 'User'
        return '[{}] {} {} ({})'.format(
            n, self.first_name, self.last_name, self.email
        )


class Contact(UserLink):
    contact_for = models.TextField(
        help_text="eg. general support / user support / server configuration. Will show up in overlays"
    )

    def __str__(self):
        return self.first_name

class FallBack(UserLink):
    notes = models.TextField(
        help_text="additional notes reguarding fallback capabilities for this user"
    )

    def __str__(self):
        return self.first_name

class Group(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, blank=True)
    description = models.TextField()

    users = models.ManyToManyField(
        'projector.User'
    )

    def __str__(self):
        return self.name
