from django.contrib import admin
from django.contrib.admin.filters import ListFilter
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group as UselessGroup
from projector.models import *
#from overlay.models import Overlay,
from markdownx.admin import MarkdownxModelAdmin


admin.AdminSite.index_template = 'admin-extra/index.html'



class UserLinkInline(admin.StackedInline):
    extra = 0

    def get_fieldsets(self, request, obj=None):
        return [
            (None, dict(fields=('user',))),
            (
                'Manual User (If no WatIAM)',
                dict(
                    fields=(
                        'override_first_name',
                        'override_last_name',
                        'override_email',
                    ),
                    classes=('collapse',)
                )
            )
        ] + self.extra_fieldsets


class ContributorInline(UserLinkInline):
    model = Contributor
    extra_fieldsets = [
        (None, dict(fields=('role', 'contributed_to',)))
    ]


class ContactInline(UserLinkInline):
    model = Contact
    extra_fieldsets = [
        (None, dict(fields=('contact_for',)))
    ]

class FallBackInline(UserLinkInline):
    model = FallBack
    extra_fieldsets = [
        (None, dict(fields=('notes',)))
    ]


class OwnProjectFilter(admin.SimpleListFilter):
    title = "Project"
    parameter_name = 'project'

    def lookups(self, request, model_admin):
        return model_admin.get_queryset(request)\
            .values_list('project__pk', 'project__title')
            #.filter(project__contact__user=request.user)\
    
    def _queryset(self, request, queryset):
        print(self.value())
        if self.value():
            return queryset.filter(project=self.value())

class LimitProjectDropdownMixin:
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.all().by_user(request.user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

#
# class NotificationInline(admin.StackedInline):
#     model = Notification
#     extra = 0

# class OverlayAdmin(LimitProjectDropdownMixin, admin.ModelAdmin):
#     list_filter = [OwnProjectFilter]
#     list_display = ['project_title']
#     inlines = [NotificationInline]
#
#     def project_title(self, obj):
#         return obj.project.title
#
#     def get_queryset(self, request):
#         qs = super().get_queryset(request)
#         #if request.user.is_superuser:
#         #    return qs
#         return qs.filter(project__in=Project.objects.all().by_user(request.user))


# class TrackedDocumentAdmin(LimitProjectDropdownMixin, MarkdownxModelAdmin):
#     list_display = ['title', 'project']
#     list_filter = [OwnProjectFilter]
#     fields = [
#         'project',
#         'title',
#         'slug',
#         'body',
#     ]
#     prepopulated_fields = {'slug': ("title",)}
#     class Media:
#         css = {'all': ['project_admin.css']}
#
#     def _get_queryset(self, request):
#         qs = super().get_queryset(request)
#         #if request.user.is_superuser:
#         #    return qs
#         return qs.filter(project__in=Project.objects.all().by_user(request.user))
#
#
#     def save_model(self, request, obj, form, change):
#         # Create revisions...
#         if change:
#             # Clone this guy...
#             original = TrackedDocument.objects\
#                 .values('author_id', 'body', 'date_updated')\
#                 .get(id=obj.id)
#             print(original)
#             DocumentVersion.objects.create(
#                 document=obj,
#                 **original
#             )
#         obj.author = request.user
#         super().save_model(request, obj, form, change)


class ProjectAdmin(MarkdownxModelAdmin):
    change_form_template = 'project_admin_change.html'
    search_fields = ['title']
    list_filter = ['groups__name']
    list_display = ['title', 'url', 'get_groups']
    fieldsets = [
        ('Project Information',     {
            'fields': ['title', 'slug', 'icon', 'tagline', 'description', 'url', 'documentation_url', 'ticket_url', 'project_created', 'visible']
        }),
        ('Project Details',    {'fields': ['scope', 'technologies', 'groups']}),
        # ('Gitlab Integration',     {
        #     'fields': ['gitlab_id', 'gitlab_access_token', 'gitlab_url'],
        #     'classes': ['collapse']

        # }),
    ]

    prepopulated_fields = {'slug': ("title",)}

    filter_horizontal = ['technologies', 'groups']
    inlines = [ContactInline, ContributorInline, FallBackInline]

    def get_groups(self, obj):
        return ", ".join(obj.groups.order_by('name').values_list('name', flat=True))

    def _get_queryset(self, request):
        qs = super().get_queryset(request)
        #if request.user.is_superuser:
        #    return qs
        return qs.by_user(request.user)
    
class CustomUserAdmin(UserAdmin):
    def has_change_permission(self, request, obj=None):
        return obj == request.user
    fieldsets = (
        (None, {'fields': ('avatar', 'username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email')}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        #(_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

class TechnologyAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ('name',)}

class GroupAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        if obj is not None:
            return obj.users.filter(id=request.user.id).exists()
        return super().has_change_permission(request, obj)
    prepopulated_fields = {"slug": ('name',)}
    def _get_queryset(self, request):
        qs = super().get_queryset(request)
        #if request.user.is_superuser:
        #    return qs
        return qs.filter(users=request.user)


admin.site.register(User, CustomUserAdmin)
#admin.site.register(TrackedDocument, TrackedDocumentAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Technology, TechnologyAdmin)
# hide these because they are more usable from the project model view.
#admin.site.register(Contributor)
#admin.site.register(Contact)
admin.site.register(Group, GroupAdmin)
admin.site.unregister(UselessGroup)


# admin.site.register(Overlay, OverlayAdmin)
# Register your models here.
