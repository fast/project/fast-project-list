from projector.models import Project, Group
# Need to hash these badboys out!

def user_projects_queryset(user):
    # Users can edit anything they're in the group for!
    return Project.objects.all().by_user(user)

def can_edit_project(user, project):
    return Project.objects.filter(id=project.id).by_user(user).exists()