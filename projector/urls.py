"""projector URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from projector import views
from projector import api
#from overlay.views import send_message, project_json_view, overlay_api
from markdownx.views import (
    ImageUploadView,
    MarkdownifyView,
)
from rest_framework import routers
try:
    from projector.local_urls import urlpatterns as custom_url_patterns
except:
    custom_url_patterns = []

#api_router = routers.SimpleRouter()
#api_router.register(r'projects', api.PublicProjectViewSet)
urlpatterns = [
    path('', include(custom_url_patterns)),
    path('', views.IndexView.as_view(), name='index'),
    #path('hooks/', include('hooks.urls')),
    #path('overlay/<slug:slug>', overlay_api, name='overlay'),
    #path('<int:pk>/json', project_json_view, name='json-detail'),
    #path('<int:pk>/send-message', send_message, name='send-message'),
    #path('<slug:slug>/send-message', send_message, name='send-message'),
    #path('<slug:slug>/json', project_json_view, name='json-detail'),
    path('spec-dump/', views.spec_dump, name='spec_dump'),
    path('about/', views.about, name='about'),
    path('admin/', admin.site.urls),
    re_path(r'^markdownx/', include([
        re_path(r'^upload/$', ImageUploadView.as_view(), name='markdownx_upload'),
        re_path(r'^markdownify/$', MarkdownifyView.as_view(), name='markdownx_markdownify'),
    ])),
    # Until we deprecate PK, both int and PK work :)
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/specifications', views.specifications, name='specifications'),
    path('<int:pk>/save_specification/<str:spec_key>/', views.save_specification, name='save_specification'),

    # NOTE Should put most new urls in here
    path('<slug:slug>/', views.DetailView.as_view(), name='detail'),
    path('<slug:slug>/specifications', views.specifications, name='specifications'),
    #path('<slug:slug>/document/<slug:doc_slug>', views.DocumentView.as_view(), name='view-document'),
    #path('api/v1/', include(api_router.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.http import urlencode
from django.conf import settings
from django.http import HttpResponse
def oidc_admin_login(request):
    if request.user.is_authenticated:
        # prevent login redirect loop when no admin access
        return HttpResponse(f'Sorry. your user does not have admin access. {request.user}')
    return redirect(f"{reverse_lazy(settings.LOGIN_URL)}?{urlencode({'next': request.GET.get('next', '/')})}")
def oidc_admin_logout(request):
    return redirect(reverse_lazy(settings.LOGOUT_URL))
if hasattr(settings, 'OIDC_CLIENTS'):
    urlpatterns = [
        path('', include('oidc_auth.urls')),
        path('admin/login/', oidc_admin_login),
        path('admin/logout/', oidc_admin_logout),
    ] + urlpatterns
