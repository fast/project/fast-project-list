#!/bin/bash
set -exu
# Run this from inside devcontainer

# alpine
if command -v apk; then
    sudo apk update
    sudo apk add postgresql-client
fi
# debian
if command -v apt; then
    sudo apt update
    sudo apt install -y postgresql-client
fi
# redhat
if command -v yum; then
    sudo yum install -y postgresql-client
fi

# Clear out DB tables and objects
PGPASSWORD=$DJANGO_DB_PASSWORD psql --host $DJANGO_DB_HOST --username $DJANGO_DB_USER --dbname $DJANGO_DB_NAME -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"

echo '
assuming you use
===================
    eval $(ssh-agent)
    ssh-add ~/.ssh/id_ed25519
    code ./
'
ssh root@fast-01-prod.fast.uwaterloo.ca \
    docker exec fast_db_1 pg_dump --no-owner --clean --if-exists --username fast --dbname fast \
    | PGPASSWORD=$DJANGO_DB_PASSWORD psql --host $DJANGO_DB_HOST --username $DJANGO_DB_USER --dbname $DJANGO_DB_NAME 
