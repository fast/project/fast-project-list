#!/usr/bin/env bash
set -exu
./manage.py collectstatic --noinput || true
./manage.py migrate --noinput

# <<EOF ./manage.py shell
# from django.contrib.auth import get_user_model
# user = get_user_model().objects.filter(username="admin").first()
# if user:
#     user.set_password('$ADMIN_PASSWORD')
#     user.save()
# else:
#     get_user_model().objects.create_superuser("admin", "admin@localhost.invalid", "$ADMIN_PASSWORD")
# EOF

./manage.py runserver 0.0.0.0:8000
#gunicorn core.wsgi --timeout 60 --log-level debug -w 4 -b 0.0.0.0:8000
