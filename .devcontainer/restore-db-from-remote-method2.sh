#!/bin/bash
set -exu
echo "WARNING: Run this script from a shell that is hosting the devcontainers not from inside of container"

# list containers
docker ps

# assuming this is your db
docker_db=fast-project-list_devcontainer-db-1

# Clear out DB tables and objects
docker exec $docker_db psql --username postgres --dbname postgres -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"

# note might need to accept knowenhost
ssh root@fast-01-prod.fast.uwaterloo.ca \
    docker exec app_db_1 pg_dump --no-owner --clean --if-exists --username postgres --dbname postgres \
    | docker exec -i $docker_db psql --username postgres --dbname postgres
