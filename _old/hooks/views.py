from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.core.mail import EmailMessage
from django.template import Template, Context
from .models import Hook, HookAccessLog

import json
# Create your views here.

def render_default(var, context_dict, *, default=''):
    if var is None:
        to_render = default
    else:
        to_render = var
    return Template(to_render).render(Context(context_dict))


def process_hook_email(data, hook):
    config = hook.configuration
    context_data = {'post_data': data, 'hook': hook}
    subject = render_default(
        config.get('subject', None),
        context_data,
        default="Hook {{hook.project.slug}}/{{hook.endpoint}} Triggered!"
    )
    body = render_default(
        config.get('body', None),
        context_data,
        default="{{post_data|safe}}"
    )
    to = config['to']
    if isinstance(to, str):
        to = [to]
    msg = EmailMessage(
        subject=subject,
        to=to,
        body=body,
        from_email='fast-hook@uwaterloo.ca'
    )
    msg.send()

def process_hook_test(data, hook):
    pass
    
HOOK_FN_LOOKUP = {
    'email': process_hook_email,
    'test': process_hook_test
}

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@csrf_exempt
@require_http_methods(['POST'])
def postHook(request, project_slug, endpoint):
    # We'll verify there's an API key in the header before we do anything else
    key = request.META.get('HTTP_KEY', None)
    if key is None:
        return JsonResponse({'error': 'No KEY header found'}, status=403)

    hook = Hook.objects\
        .filter(project__slug=project_slug, endpoint=endpoint)\
        .select_related('project')\
        .first()
    
    if hook is None:
        return HttpResponse(status=404)
    print(key, hook.project.slug)
    if hook.project.api_key != key:
        return JsonResponse(
            {'error': 'Invalid Key'},
            status=403
        )
    
    # Process...
    log = ''
    status = 'OK'
    data = None
    try:
        data = request.body.decode('utf-8')
        if request.META.get('CONTENT_TYPE', '') == 'application/json':
            data = json.loads(data)
        HOOK_FN_LOOKUP[hook.hook_type](data, hook)
    except Exception as e:
        log = str(e)
        status = 'ERR'
    
    HookAccessLog.objects.create(
        log=log,
        status=status,
        payload=request.body,
        request_ip=get_client_ip(request),
        hook=hook
    )
    return HttpResponse(status=200) if status == 'OK' else JsonResponse({'error': log},status=500)
        


    



