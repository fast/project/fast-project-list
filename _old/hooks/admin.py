from django.contrib import admin
from .models import Hook, HookAccessLog
from projector.admin import OwnProjectFilter, LimitProjectDropdownMixin
from django.db.models import F
# Register your models here.

class HookFilter(admin.SimpleListFilter):
    title = "Hook"
    parameter_name = 'hook'

    def lookups(self, request, model_admin):
        return [(x[0], "{}/{}".format(x[1], x[2]))
            for x in model_admin.get_queryset(request)
            .filter(hook__project__contact__user=request.user)
            .values_list('hook__pk', 'hook__project__slug', 'hook__endpoint')
        ]
    
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(hook=self.value())

class HookProjectFilter(admin.SimpleListFilter):
    title = "Project"
    parameter_name = 'project'

    def lookups(self, request, model_admin):
        return model_admin.get_queryset(request)\
            .filter(hook__project__contact__user=request.user)\
            .values_list('hook__project__pk', 'hook__project__title')
    
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(hook__project=self.value())

@admin.register(Hook)
class HookAdmin(LimitProjectDropdownMixin, admin.ModelAdmin):
    list_filter = [OwnProjectFilter]
    list_display = ['endpoint', 'project_title']


    def project_title(self, obj):
        return obj.project.title

    project_title.admin_order_field = 'project__title'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(project__contact__user=request.user).select_related('project')

@admin.register(HookAccessLog)
class HookAccessLogAdmin(admin.ModelAdmin):
    list_filter = [HookProjectFilter, HookFilter]
    list_display = ['timestamp', 'status', 'project', 'endpoint']


    def has_change_permission(*args, **kwargs):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(hook__project__contact__user=request.user)\
            .annotate(project=F('hook__project__title'))\
            .annotate(endpoint=F('hook__endpoint'))
    
    def project(self, obj):
        return obj.project
    
    def endpoint(self, obj):
        return obj.endpoint
