# FAST Hooks

Mirko got it into his head this would be a good idea, and thus it now exists. FAST now supports very basic hooks which trigger various actions. At the moment this is only being used for sending emails on failed cron jobs.

A python3 script is provided in this directory `fast_hook`, which can be run as follows:

```
fast_hook -k <project key> -e fast/endpoint -v key value -v key2 value2 '<command>'  
```

The project keys are available in the admin panel for a given fast projct, and hook endpoints are `<project_slug>/<endpoint>`.

The script, by default, will send `stderr` to the endpoint. Any `-v` flags will be included in the JSON data posted to the hook. If you'd like to include `stdout` in the post add an `-o` flag to the command. See `fast_hook --help` for more info.

## Setting Up Hooks

Right now the hooks are super limited in what they can do and who can set them up. They only support email, and configuration needs to be done via manual JSON.

> This workflow may be improved in the future if the feature is used my multiple parties. Notably adding a UI for editing the stupid config.

Note: **you can only edit hooks for projects you've been set as a contact for**. This was mostly done to reduce the amount of noise in the UI.

Here's an example hook setup:

```
project:      fast
endpoint:     notify
type:         email
configuration:
{
    "to": "mvucicev@uwaterloo.ca",
    "body": "The command {{post_data.command|safe}} failed with the following output: {{post_data.stderr|safe}}",
    "subject": "Alert: [{{title}}] process failed." 
}
```

> body and subject can be left blank and some defaults will be used.

Then hit the hook with the following:
```
fast_hook -v title 'Test Command' -k <key> -e fast/notify 'ls /zzz'
```

The command should fail, and the email 

## I don't want to use your stupid wrapper script
That's fine! You can just use curl or something:

```
curl -X POST  https://fast.uwaterloo.ca/hooks/<project>/<endpoint> -H 'key:<key>'  
```

## Why didn't you just write a wrapper script that sent you emails or used CRON's emailing system?
Hopefully this can be extended -- it should allow for way more options 

## What if I want a hook endpoint not attached to a specific project?
I recommend creating a new "invisible" project -- create a new project and simply set "Show on FAST Website" to false.


