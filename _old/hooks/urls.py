from django.urls import path
from .views import postHook

urlpatterns = [
    path('<slug:project_slug>/<slug:endpoint>', postHook)
]