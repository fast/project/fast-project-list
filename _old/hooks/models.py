from django.db import models
HOOK_TYPE_OPTIONS = (
    ('test', 'Test (only logs)'),
    ('email', 'Send Email'),
)


LOG_STATUS_CHOICES = (
    ('OK', 'Success'),
    ('ERR', 'Error'),
)
class Hook(models.Model):
    project = models.ForeignKey(
        'projector.Project', on_delete=models.CASCADE,
        help_text="Note: limited to projects for which you are a contact"
    )
    description = models.TextField(blank=True)
    endpoint = models.SlugField(
        max_length=256,
        help_text="This will be the endpoint to target"
    )
    hook_type = models.CharField(
        choices=HOOK_TYPE_OPTIONS,
         default='test',
         max_length=64
    )
    configuration = models.JSONField()

    def __str__(self):
        return "{}/{}".format(self.project.slug, self.endpoint)

class HookAccessLog(models.Model):
    hook = models.ForeignKey('hooks.Hook', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    request_ip = models.GenericIPAddressField(blank=True, null=True)
    payload = models.TextField(blank=True)
    status = models.CharField(choices=LOG_STATUS_CHOICES, default='OK', max_length=16)
    log = models.TextField(blank=True)
