from django.db import models
from django.utils import timezone


class Overlay(models.Model):
    project = models.OneToOneField("projector.Project", on_delete=models.CASCADE)
    title_override = models.CharField(
        help_text="Force a different title in the 'Get Help for X' part of the modal",
        blank=True,
        max_length=256,
    )
    show_notifications = models.BooleanField(
        help_text="If true, notifications set here will pop up in your overlay",
        default=True,
    )
    show_contacts = models.BooleanField(
        help_text="Email addresses and names of contacts will show up on the overlay modal",
        default=True,
    )
    show_documentation_link = models.BooleanField(
        help_text="If the documentation url is set in your project, it will show up on the overlay modal",
        default=True,
    )

    show_ticket_link = models.BooleanField(
        help_text="If the ticketing system url is set in your project, it will show up on the overlay modal",
        default=True,
    )

    jira_issue_collector_url = models.TextField(
        help_text="If you have a Jira Issue Collector configured, paste the URL for it here.",
        blank=True,
    )

    @property
    def active_notifications(self):
        return self.notifications.filter(
            models.Q(
                models.Q(active_start__lte=timezone.now()) |
                models.Q(active_start=None)
            ) &
            models.Q(
                models.Q(active_end__gte=timezone.now()) |
                models.Q(active_end=None)
            )
        )



class Notification(models.Model):
    overlay = models.ForeignKey(
        'overlay.Overlay',
        related_name='notifications',
        on_delete=models.CASCADE
    )
    highlight = models.CharField(
        choices=(
            ( 'default','default (no color)' ),
            ('success', 'success (green)'),
            ('info', 'info (blue)'),
            ('warning', 'warning (yellow/orange)'),
            ('danger', 'danger (red)'),
        ),
        max_length=64,
        default='default'
    )
    title = models.CharField(
        blank=True, max_length=256
    )
    content = models.TextField()
    active_start = models.DateTimeField(
        help_text="[Optional] alert won't show until this time",
        blank=True,
        null=True
    )
    active_end = models.DateTimeField(
        help_text="[Optional] alert will stop showing at this time. Expired alerts will auto-delete eventually.",
        blank=True,
        null=True
    )

    def __str__(self):
        return self.title

