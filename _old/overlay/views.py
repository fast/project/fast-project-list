from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied, FieldError
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMessage
from django.template import *
from django.template import loader
from django.http import JsonResponse, Http404
from projector.models import Project
from urllib.parse import urlparse
from overlay.models import Notification
import json
import re

domain_match = re.compile(r'(http(s)?://)?(www\.)?(\w+(?:\.\w+)+)(:\d+)?')


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def makeError(message, status=400, detail=None):
    return JsonResponse(dict(message=message, detail=detail), status=status)

@csrf_exempt
def send_message(request, pk):
    # Get project (either put pk in request.POST or in the url)
    # add "hostnames" to project model (comma sepatared)
    # get all hostnames from project, figure out how to split them
    # project.hostnames: pygmy.uwat.ca,something.something.pa,other.com
    # check if incoming hostname/domain is valid
    # In the javascript, add an "alert" if m.request fails (in the catch)
    project = get_object_or_404(Project, pk=pk)
    referrer = urlparse(request.META['HTTP_REFERER'])
    referrer = "{}://{}{}".format(
        referrer.scheme,
        referrer.hostname,
        ':{}'.format(referrer.port) if referrer.port else ''
    )
    project_domain = domain_match.search(project.url).group(4)
    referrer = domain_match.search(referrer).group(4)
    if referrer != project_domain or project_domain is None:
        return makeError('This website is not authorized to send emails.')

    context = {
        'browser': request.POST.get('browser'),
        'ip_address': get_client_ip(request),
        'url': request.POST.get('url'),
        'name': request.POST.get('name', None),
        'email': request.POST.get('email', None),
        'comment': request.POST.get('comment', None),
    }

    if not context['comment'] or not context['email']:
        return makeError('Please complete all fields')

    email = EmailMessage(
        'New Support Request: {}'.format(project.title),
        loader.get_template('email.html').render(context),
        'noreply@uwaterloo.ca',
        [request.POST.get('receiver')],
        reply_to=[
            '{} <{}>'.format(
                request.POST.get('name'),
                request.POST.get('email'),
            )
        ]
    )

    email.content_subtype = 'html'
    email.attach('screenshot.png', request.FILES.get('screenshot').file.read())
    try:
        email.send()
    except:
        return makeError('Failed to send email (bad configuration)', 500)

    return JsonResponse(context)


def project_json_view(request, pk):
    project = get_object_or_404(Project, pk=pk)
    notices = project.overlay.active_notifications if hasattr(project, 'overlay') else []
    contacts = project.contact_set.all()

    output = {
        "title": project.title,
        "contacts": [
            {
                'fullname': '{} {}'.format(x.first_name, x.last_name),
                'email': x.email,
                'contact_for': x.contact_for
            } for x in contacts
        ],
        "notices": [
            {
                'title': x.title,
                'content': x.content,
                'id': x.id,
            } for x in notices
        ]
    }

    return JsonResponse(output)

def overlay_api(request, slug):
    project = Project.objects.filter(slug=slug).select_related('overlay').first()
    if project is None:
        raise Http404
    ol = getattr(project, 'overlay', None)
    if ol is None:
        raise Http404
    output = {
        "title": ol.title_override if ol.title_override else project.title,
        "contacts": [
            {
                'fullname': '{} {}'.format(x.first_name, x.last_name),
                'email': x.email,
                'contact_for': x.contact_for
            } for x in project.contact_set.all()
        ] if ol.show_contacts else [],
        "notifications": [
            {
                'title': x.title,
                'content': x.content,
                'id': x.id,
                'highlight': x.highlight
            } for x in ol.active_notifications
        ] if ol.show_notifications else [],
        "documentation_url": project.documentation_url if ol.show_documentation_link else '',
        "ticket_url": project.ticket_url if ol.show_ticket_link else '',
        "jira": ol.jira_issue_collector_url
    }

    return JsonResponse(output)