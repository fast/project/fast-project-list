export function pj(s){
  return s.replace(/\./gi, '.PJOVERLAY-')
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    if (exdays !== undefined){
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString() + ';';
    } else  { var expires = '' }
    document.cookie = cname + "=" + cvalue + ";" + expires + "path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function getBrowser() {
  return navigator.userAgent
}

export function getUrl(){
  var scripts = document.getElementsByTagName('script')
  var this_script

  // try to find the script via 'fast-overlay'
  // this is extremely sketchy
  for (var i=0; i<scripts.length; i++){
    var src = scripts[i].getAttribute('src')
    if(src && src.includes('fast-overlay.js')){
      if(src.startsWith('/')) return '';
      var url = new URL(src)
      url = `${url.protocol}//${url.host}`
      return url
    }
  }
  console.error('Unable to find script with source containing fast-overlay.js!')
}
