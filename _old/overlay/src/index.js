import m from 'mithril';
import {pj, getCookie, setCookie, getBrowser, getUrl} from './utils.js';
import Modal from './components/modal'
import nModal from './components/newmodal'
import ContactList from './components/contactlist'
import html2canvas from 'html2canvas';
import './base.css'


const State = {
  data: null,
  selected_contact: null,
  contactlist: false,
  base_url: null,
  project_id: null,

  config: {
    notifications: {
      timeout: 0
    }
  }
}

const Actions = {
    getData: function(){
         m.request({
            'url': State.base_url + "/" + State.project_id+'/json',
            'method': 'GET',
        }).then(function(response){
            response.notices = response.notices.filter(function(n){
                var seen = getCookie('P-notice-'+n.id)
                return seen == ''
            })
            State.data = response
        }).catch(function(error){
            console.error('Failed to retrieve data from FAST site: ' + error.response)
        })
    },
    deleteNotification: function(notice){
        setCookie('P-notice-' + notice.id, 'closed')
        State.data.notices = State.data.notices.filter(function(x){
            return x.id != notice.id
        })
    },
    selectContact: function(info){
      State.contactlist = false
      State.selected_contact = info
    }
}

const HelpBall = {
  view: vnode => m(pj('.help-ball'), {
    onclick: e=>{State.contactlist = true}
  }, '?')
}

const Notification = function(init){
    var info = init.attrs.info
    var timeout = null
    var time = 1000 * (15 + (5 * init.attrs.index))

    return {
        oncreate: function(vnode){
            if (State.config.notifications.timeout == 0) return;
            timeout = setTimeout(function(){
                Actions.deleteNotification(info)
                m.redraw()
            }, time)
        },
        view: function(vnode){
            return m(pj('.notification'), [
                m(pj('.closebutton'), {
                    onclick: () => {
                        Actions.deleteNotification(info)
                    }
                }, 'x'),
                m('p', m('b[style="font-size:120%;"]', info.title)),
                info.content.split('\n').map(x=>m('p', x))
            ])
        }
    }
}

const Notifications = function(init){
    return {
        view: function(vnode){
            return m(pj('.notification-list'), [
                State.data.notices.map(function(info, i){
                    return m(
                        Notification, {info: info, key: info.id, index: i})
                })
            ])
        }
    }
}

const Form = function(){
  var form = null
  var screenshot = null
  var sending = false
  return {
    view: function(vnode){
      return [
      sending && m(pj('.modal-cover'), m('div', m('h4', 'Sending...'), m(pj('.dual-ring')))),
      m(pj('span.closebutton'), {onclick: ()=>{State.selected_contact=null}}, m.trust('&times;')),
      m(pj('form.form'), {
        oncreate: function(vnode){
          form = vnode.dom
        }
      }, [
        m('h4', 'Request Support from ', State.selected_contact.fullname),
        m('p', m('em',
          '(You may also contact this person manually via ',
          m('a', {href: 'mailto:'+State.selected_contact.email}, State.selected_contact.email),
          ')'
        )),
        m('p', 'This process will send an email to ',
          State.selected_contact.fullname,
          '. Please provide accurate information below to help resolve the issue quicker.'
        ),
        m('div', [
          m('span', 'Your name:'),
          m('br'),
          m('input[type="text"][name="name"]'),
          m('br'),
        ]),

        m('div', [
          m('span', 'Your Email address:'),
          m('br'),
          m('input[type="text"][name="email"]'),
          m('br'),
        ]),

        m('div', [
          m('span', 'Describe the issue:'),
          m('br'),
          m('textarea#comment[rows="8"][name="comment"]',),
          m('br'),
        ]),
        m('hr'),
        m('p', 'To assist in resolving the issue, the following information will ',
          m('strong', 'automatically'), ' be added to the message:'),
        m('ul',
          m('li', 'Your IP address'),
          m('li', 'Your web browser + version'),
          m('li', 'The current URL'),
          m('li', 'A screen-capture of this page (only within your browser)'),
        ),
        
        m('input[type="button"][value="Send"]', {
          onclick: function(){
            sending = true
            html2canvas(document.body, {
              allowTaint: true,
              ignoreElements: (el)=>{
                return (
                  el.classList.contains('PJOVERLAY-overlay') || 
                  el.classList.contains('PJOVERLAY-help-ball') || 
                  el.classList.contains('PJOVERLAY-help-contactlist') || 
                  el.classList.contains('PJOVERLAY-help-modal')
                )
              }
            }).then(function(canvas) {
              var blobBin = atob(canvas.toDataURL().split(',')[1]);
              var array = [];
              for(var i = 0; i < blobBin.length; i++) {
                  array.push(blobBin.charCodeAt(i));
              }
              screenshot=new Blob([new Uint8Array(array)], {type: 'image/png'});
             
              var form_data = new FormData(form)
              form_data.append("screenshot", screenshot)
              form_data.append("url", window.location.href)
              form_data.append("browser", getBrowser())
              form_data.append("receiver", State.selected_contact.email)

              m.request({
                url: State.base_url + '/' +State.project_id + '/send-message',
                body: form_data,
                method: 'POST'
              }).then(()=>{
                sending = false
                alert("Email Sent!")
                State.selected_contact = null
              }).catch((error)=>{
                sending = false
                alert("Error!: " + error.response.message ? error.response.message : error.code)
              })
            });
          }
        }),
      ])
      ]
    }
  }
}

const App = {
  oninit: function(vnode){
      Actions.getData()
  },
  view: function(vnode){
    if (State.data === null) return null;
    return [
      m(HelpBall),
      State.contactlist && m(ContactList, {
        removeFn: ()=>{State.contactlist = false},
        contacts: State.data.contacts,
        clickContact: Actions.selectContact,
      }),
      m(Notifications),
      State.selected_contact && m(nModal, {
        class: 'PJOVERLAY-modal PJOVERLAY-animated PJOVERLAY-fadeInUp',
        overlay_class: 'PJOVERLAY-animated PJOVERLAY-overlay PJOVERLAY-fadeIn',
        overlay_fade_out_class: 'PJOVERLAY-fadeOut',
        removeFn: () => {State.selected_contact = null}
      }, m(Form)),
    ]
  }
}

window.projector = {
  mount: function(config){
    State.config = config
    State.project_id = config.id
    State.base_url = getUrl()
    if (!State.base_url && State.base_url !== '') return;
    const elm = document.createElement('DIV')
    document.body.appendChild(elm)
    m.mount(elm, App)
  }
}
