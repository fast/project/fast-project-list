import m from 'mithril'

export default function Modal(init){
  const modal_class = init.attrs.class
  const fade_out_class = init.attrs.fade_out_class
  const overlay_class = init.attrs.overlay_class
  const overlay_fade_out_class = init.attrs.overlay_fade_out_class
  const removeFn = init.attrs.removeFn

  var dom = null
  var overlaydom = null

  const clickOutside = e => {
    if(!dom.contains(e.target)) removeFn()
    m.redraw()
  }
  return {
    onbeforeremove: vnode => {
      dom.classList.add(fade_out_class || 'n-')
      overlaydom.classList.add(overlay_fade_out_class || 'n-')
      return new Promise(r => {
        overlaydom.addEventListener('animationend', r)
      })
    },
    view: vnode =>  m('div', {
        class: overlay_class,
        oncreate: vnode => {
          overlaydom = vnode.dom
        }
      },
      m('div', {
        class: modal_class,
        oncreate: vnode => {
          dom = vnode.dom
          document.addEventListener('click', clickOutside)
        },
        onremove: vnode => {
          document.removeEventListener('click', clickOutside)
        }
      }, vnode.children)
    )
  }
}
