import m from 'mithril'
import {pj} from '../utils'

const Contact = {
  view: vnode => {
    const info = vnode.attrs.info
    return m(pj('.contact'), {
      onclick: e => {
        vnode.attrs.onclick(info)
      }
    },
      m('strong', info.fullname),
      ' (' + info.email + ')',
      m(pj('.muted'), info.contact_for)
    )
  }
}

export default function ContactPopup(init){
  var dom = null
  const removeFn = init.attrs.removeFn
  function clickOutside(e){
    if (!dom.contains(e.target)) removeFn()
    m.redraw()
  }

  return {
    onbeforeremove: vnode => {
      dom.classList.add('PJOVERLAY-slideOutDown')
      return new Promise(r => {
        dom.addEventListener('animationend', r)
      })
    },
    view: (vnode)=>m(pj('.contacts.animated.slideInUp'), {
      oncreate: vnode => {
        dom = vnode.dom
        document.addEventListener('click', clickOutside)
      },
      onremove: vnode => {
        document.removeEventListener('click', clickOutside)
      }
    },
      m('h4', 'Request Support'),
      vnode.attrs.contacts.map(contact=>m(Contact, {
        info: contact,
        onclick: vnode.attrs.clickContact
      }))
    )
  }
}

