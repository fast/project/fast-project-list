import m from 'mithril'

const Overlay = function() {
	let dom
	let children

	const OverlayContainer = {
		view: () => children
	}

	return {
		oncreate(v) {
			children = v.children
			// Append a container to the end of body
			dom = document.createElement('div')
            dom.className = 'pj-overlay'
            dom.setAttribute('data-html2canvas-ignore', true)
			document.body.appendChild(dom)
			m.mount(dom, OverlayContainer)
		},
		onbeforeupdate(v) {
			children = v.children
		},
		onbeforeremove(v) {
			// Add a class with fade-out exit animation
			dom.classList.add('hide')
			return new Promise(r => {
				dom.addEventListener('animationend', r)
			})
		},
		onremove() {
			m.mount(dom, null)
			// Destroy the overlay dom tree. Using m.mount with
			// null triggers any modal children removal hooks.
			document.body.removeChild(dom)
		},
		view() {}
	}
}

const Modal = function(v) {
    let clickedId
    function bodyclick(e){
      if (e.target.closest('.PJ-modal') == null){
        clickedId = 'close'
        m.redraw()
      }
    }
    return {
      view({attrs: {title, content, buttons, onClose}}) {
        if (clickedId != null) {
          // We need to allow the Overlay component execute its
          // exit animation. Because it is a child of this component,
          // it will not fire when this component is removed.
          // Instead, we need to remove it first before this component
          // goes away.
                  // When a button is clicked, we omit the Overlay component
          // from this Modal component's next view render, which will
          // trigger Overlay's onbeforeremove hook.
          return null
        }
        return m(Overlay,
          {
            oncreate(){
              document.body.addEventListener('click', bodyclick)
            },
            onremove() {
              // Wait for the overlay's removal animation to complete.
              // Then we fire our parent's callback, which will
              // presumably remove this Modal component.
              document.body.removeEventListener('click', bodyclick)
              Promise.resolve().then(() => {
                onClose(clickedId)
                m.redraw()
              })
            }
          },
          m('.PJ-modal',
            m('h3', title),
            m('.PJ-modal-content', content),
            m('.PJ-modal-buttons',
              buttons.map(b =>
                m('button',
                  {
                    type: 'button',
                    disabled: clickedId != null,
                    onclick() {
                      clickedId = b.id
                    }
                  },
                  b.text
                )
              )
            )
          )
        )      
      }
    }
  }

  export default Modal
