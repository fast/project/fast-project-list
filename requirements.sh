#!/bin/bash

command -v apt && sudo apt install -y python3-venv \
    gcc make \
    python3-dev libldap2-dev libsasl2-dev libpq-dev libssl-dev
